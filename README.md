# My first Flask API

This is the planetary API with limited RESTful endpoints:

## Implementation details

- /planets/ - get the list of planets
- /planets/?q={term} - filter the list of planets starting with term
- /planets/{name} - get planet information by name

## Sanitising parameters

- every get parameter that is not whitelisted would be removed on page load
- every empty parameter would get erased as well
- all keys are converted to lowercase

### Running on bare metal

```
    pip install -r requirements.txt
    bash start_server.sh
```

```bash
curl http://SERVER:7890/planets/
["mars", "earth", "uranus"]
```

### Running in container without compose

```
    podman build .
    podman run -P my-first-flask-api_web # default port is used
```

### Running in container with compose

```
    podman-compose build
    podman-compose up # exact port from compose file is used
```
