import database

from flask import Flask, json, redirect, Response, request
from typing import Optional
from urllib.parse import urlencode
from werkzeug.local import LocalProxy as ReqObj
from werkzeug.wrappers.response import Response as RespObj

app = Flask(__name__)


def _sanitised_context(
    request: ReqObj, whitelist: Optional[list] = None
) -> dict | RespObj:
    """
    This would ensure our URL has only meaningful parameters
    with lowercase key names allowed by whitelist.
    Otherwise, redirect removes extra params fixing the URL.
    """

    def _urlencoded_get_params(get_params: dict) -> str:
        return f"{urlencode(get_params)}" if len(get_params) > 0 else ""

    params = dict(request.args.items())
    normalised_params = {k.lower(): v for (k, v) in params.items() if params[k]}
    if whitelist is not None:
        allowed_params = {
            k: v
            for (k, v) in normalised_params.items()
            if k in whitelist  # sanitise get params by whitelist
        }
    else:
        allowed_params = normalised_params

    optimal_path = f"{request.path}?{_urlencoded_get_params(allowed_params)}"
    if request.full_path != optimal_path:
        return redirect(optimal_path)

    return allowed_params


def _json_response(meta: dict, data: dict | list) -> Response():
    if "params" in meta and len(meta["params"]) == 0:
        meta.pop("params")

    if isinstance(data, list):
        meta["count"] = len(data)

    return Response(
        json.dumps({"meta": meta, "data": data}),
        status=200,
        mimetype="application/json",
    )


@app.route("/")  # just for convenience
def root():
    return redirect("/planets/")


@app.route("/planets/")
def planets(q: str = ""):
    """
    This is the method for "/planets/" index endpoint.
    The simple search happens if get paramater "q" is present.
    """

    # begin cleanup
    context = _sanitised_context(request, ["q"])
    if isinstance(context, RespObj):
        return context
    # end cleanup

    params = context

    planet_list = list(database.planets().keys())
    planet_list.sort()  # sort them alphabetically

    query = params.get("q", None)
    if query:
        planet_list = [pl for pl in planet_list if pl.lower().startswith(query)]

        # # Maybe redirect to SHOW page if filtered index has single result?
        # if len(planet_list) == 1:
        #     return redirect(f"/planets/{planet_list[0]}")

    # apply modifications to data according to params
    return _json_response({"params": params}, planet_list or [])


@app.route("/planets/<string:slug>/")
def planet(slug: str = ""):
    slug = slug.lower()

    # begin cleanup
    context = _sanitised_context(request, [])
    if isinstance(context, RespObj):
        return context
    # end cleanup

    params = context

    planet_info = database.planets().get(slug, None)
    # apply modifications to data according to params
    return _json_response({"params": params, "slug": slug}, planet_info or {})
