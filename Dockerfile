FROM alpine:3.16
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache bash python3 python3-dev py3-wheel \
		&& ln -sf python3 /usr/bin/python

WORKDIR /app

RUN python3 -m ensurepip \
	&& python -m pip install --upgrade pip \
	&& pip3 install --no-cache --upgrade pip

COPY requirements.txt ./
RUN pip3 install -r ./requirements.txt

COPY . .

EXPOSE 7890
CMD [ "/bin/bash", "./start_server.sh" ]
